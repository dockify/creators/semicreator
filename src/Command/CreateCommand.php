<?php

declare(strict_types=1);

namespace Dockify\Creator\Semicreator\Command;

use Dockify\Creator\Semicreator\CreatorTemplate\AbstractTemplate;
use Dockify\Creator\Semicreator\CreatorTemplate\PhpCreatorTemplate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCommand extends Command
{
    const CMD_NAME = 'Create Dockify Creator';

    public function configure()
    {
        $this->setName(self::CMD_NAME);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $availableCreatorTemplates = [
            'php-creator' => new PhpCreatorTemplate($io),
        ];

        $selectedTemplate = $io->choice(
            'Select creator template to scaffold',
            array_keys($availableCreatorTemplates)
        );

        /** @var AbstractTemplate $template */
        $template = $availableCreatorTemplates[$selectedTemplate];

        $creatorDirectoryName = $io->ask(
            'Select creator directory name',
            'my-creator'
        );

        chdir('/app');

        shell_exec(sprintf(
            'git clone %s %s',
            $template->getUrl(),
            $creatorDirectoryName
        ));

        chdir('/app/' . $creatorDirectoryName);

        $template->configure();

        $io->success('Creator has been successfully initialized');
    }
}
