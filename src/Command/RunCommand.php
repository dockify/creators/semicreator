<?php

declare(strict_types=1);

namespace Dockify\Creator\Semicreator\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunCommand extends Command
{
    /**
     * @var string
     */
    const COMMAND_NAME = 'run';

    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @return void
     */
    public function configure()
    {
        $this->setName(self::COMMAND_NAME);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);

        $this->greetUser();

        $availableCommands = [
            CreateCommand::CMD_NAME,
            LeaveCommand::CMD_NAME,
        ];

        $userSelectedCommand = $this->io->choice(
            'What would you like to do?',
            $availableCommands
        );

        $this->getApplication()
            ->find($userSelectedCommand)
            ->run(new ArrayInput([]), $output);
    }

    protected function greetUser(): void
    {
        $greetings = [
            'Ahoy!',
            'Hello!',
            'Hi there!',
            'Howdy, partner!',
        ];

        $selectedGreeting = $greetings[array_rand($greetings)];

        $this->io->title($selectedGreeting);
    }
}
