<?php

declare(strict_types=1);

namespace Dockify\Creator\Semicreator\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LeaveCommand extends Command
{
    const CMD_NAME = 'Leave';

    const BYE_MESSAGES = [
        'Bye!',
        'See you soon!',
        'See you later!',
    ];

    public function configure()
    {
        $this->setName(self::CMD_NAME);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $byeMessage = self::BYE_MESSAGES[array_rand(self::BYE_MESSAGES)];

        $output->writeln("<info>$byeMessage</info>");
    }
}
