<?php

declare(strict_types=1);

namespace Dockify\Creator\Semicreator\CreatorTemplate;

class PhpCreatorTemplate extends AbstractTemplate
{
    protected $url = 'https://gitlab.com/dockify/creator-templates/php-creator-template';

    public function configure(): void
    {
        $creatorFullName = $this->io->ask(
            'Enter creator full name',
            'my-vendor/new-creator'
        );

        $creatorNameChunks = explode('/', $creatorFullName);

        $creatorName = end($creatorNameChunks);

        $creatorAuthorName = $this->io->ask(
            'Enter creator author full name',
            'John Doe'
        );

        $creatorAuthorEmail = $this->io->ask(
            'Enter creator author email',
            'JohnDoe@example.com'
        );

        $creatorDescription = $this->io->ask(
            'Enter creator description',
            'Creator for creating new things'
        );

        $creatorRootNamespace = trim($this->io->ask(
            'Enter creator root namespace',
            'Dockify\ThirdPartyCreator\NewCreator'
        ), '\\');

        $creatorRegistry = $this->io->ask(
            'Enter creator docker registry',
            'registry.gitlab.com/dockify/third-party/new-creator'
        );

        $replacements = [
            '%creator_name%' => $creatorName,
            '%creator_full_name%' => $creatorFullName,
            '%creator_description%' => $creatorDescription,
            '%creator_root_namespace%' => $creatorRootNamespace,
            '%creator_root_namespace_escaped%' => str_replace(
                '\\',
                '\\\\',
                $creatorRootNamespace
            ),
            '%creator_registry%' => $creatorRegistry,
            '%creator_author_name%' => $creatorAuthorName,
            '%creator_author_email%' => $creatorAuthorEmail,
        ];

        $filesToExpand = array_merge(
            glob('src/*.php'),
            glob('src/**/*.php'),
            [
                'cmd.sh',
                'manifest.json',
                'composer.json',
                'application.php',
                '.gitlab-ci.yml.example',
            ]
        );

        foreach ($filesToExpand as $filePath) {
            if (file_exists($filePath)) {
                $fileContent = file_get_contents($filePath);

                if (is_string($fileContent)) {
                    file_put_contents(
                        $filePath,
                        str_replace(
                            array_keys($replacements),
                            array_values($replacements),
                            $fileContent
                        )
                    );
                }
            }
        }
    }
}
