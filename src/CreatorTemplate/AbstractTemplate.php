<?php

declare(strict_types=1);

namespace Dockify\Creator\Semicreator\CreatorTemplate;

use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractTemplate
{
    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @var string
     */
    protected $url = 'https://example.com';

    public function __construct(SymfonyStyle $io)
    {
        $this->io = $io;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    abstract public function configure(): void;
}
