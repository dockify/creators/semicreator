#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Dockify\Creator\Semicreator\Command\RunCommand;
use Dockify\Creator\Semicreator\Command\LeaveCommand;
use Dockify\Creator\Semicreator\Command\CreateCommand;

$application = new Application();
$application->add(new RunCommand());
$application->add(new LeaveCommand());
$application->add(new CreateCommand());

try {
    $application->run();
} catch (\Exception $exception) {
    echo sprintf('An error occurred during creator execution: %s', $exception->getMessage());
}
