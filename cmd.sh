#!/usr/bin/env bash

# Constants
DEV_IMAGE_NAME="dockify/semicreator:dev"

# ==========================
# ===  HELPER FUNCTIONS  ===
# ==========================

build_image() {
    docker build \
        -t $DEV_IMAGE_NAME \
        -f docker/dev/Dockerfile \
        .
}

build_if_not_exists() {
    EXISTING_IMAGES=$(docker image ls --format "{{.Repository}}:{{.Tag}}" | grep $DEV_IMAGE_NAME)

    if [[ -z $EXISTING_IMAGES ]]; then
        echo "Dev image for $DEV_IMAGE_NAME not found, building it first..."
        build_image
    fi
}

exec_command() {
    build_if_not_exists

    docker run \
        -it \
        -v $PWD:/data \
        -v $PWD/storage/tmp:/app \
        -v $PWD/storage/cache:/cache \
        -e COMPOSER_HOME=/cache/composer \
        --rm \
        --user $UID:$UID \
        --entrypoint /bin/sh \
        --name semicreator \
    "$DEV_IMAGE_NAME" -c "$@"
}

# ==================
# ===  COMMANDS  ===
# ==================

case "$1" in
    "exec")
        exec_command "${@:2}"
    ;;

    "build")
        build_image
    ;;
esac
